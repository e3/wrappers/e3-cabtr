import argparse
from epics import PV
from pathlib import Path
import logging
import sys
from time import sleep


def send_file(
    device_name: str, filenames: list, verbose: bool, delay: float
):
    if verbose:
        logging.basicConfig(format="%(asctime)s: %(message)s", level=logging.INFO)

    trigger = PV(device_name + ":UploadCalibFileCmd")
    cof_pv = PV(device_name + ":CofDataToUpload.VAL")
    name_pv = PV(device_name + ":CofNameToUpload.VAL")
    reply_pv = PV(device_name + ":UploadCalibFileReply.VAL")
    http_resp = PV(device_name + ":UploadCalibFileHttpCode")

    for filename in filenames:
        p = Path(filename)

        logging.info(f"File to send: {p}")

        if not p.is_file():
            logging.error(f"Error: {p} does not exist.")
            continue

        with open(p, "r") as f:
            logging.info("Sending file contents...")
            cof_pv.put(f.read())
            logging.info("Sent!")

        logging.info(f"Updating filename PV to {p.name}")
        name_pv.put(p.name)

        logging.info(f"Sending data to cabtr device {device_name}")
        trigger.put(1)
        sleep(delay)

        logging.info(f"HTTP Response: {http_resp.get()}")
        logging.info(str(reply_pv.get(as_string=True)))


def main(sys_args):
    parser = argparse.ArgumentParser("Sends a file via EPICS cabtr module")

    parser.add_argument("device_name", help="Name of cabtr device to send calibration file to")
    parser.add_argument("filenames", nargs="+", help="File to send")
    parser.add_argument("-v", "--verbose", action="store_true", help="Verbose output")
    parser.add_argument(
        "--delay", type=float, default=1.0, help="Delay after each send"
    )

    args = parser.parse_args(sys_args)

    send_file(**vars(args))


if __name__ == "__main__":
    main(sys.argv[1:])
