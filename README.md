# e3-cabtr

EPICS cabtr module

---

## Overview

This module allows a user to communicate with a CABTR device and to send curve file data directly to it
via Channel Access. The documentation is available [here](cabtr/doc/CABTR.pdf)

## Usage

There are two parts to this module:
1. An e3 module which can send data to CABTR devices, together with an st.cmd to start the IOC
2. A python(3) script to manage sending the given data

### The e3 module

In order to run the IOC, you should either ensure that the module is installed in your e3 installation
or run a local cell install with
```
$ make cellinstall
```
If you do a local install, you will need to add the argument `-l cellMods` to the startup command below.

There is an example startup script in [cmds/st.cmd](cmds/st.cmd) that you can start with the command
```
$ iocsh.bash cmds/st.cmd
```
This will start up an IOC which will communicate with a CABTR device.

#### .iocsh files

[cabtr_monitor.iocsh](iocsh/cabtr_monitor.iocsh) sets up communication with the device and loads necessary EPICS databases. Parameters:

*   **DEVICENAME**
    *   ESS name of the device
    *   string
*   **IPADDR**
    *   The hostname or IP address of the device
    *   string
*   **POLL**
    *   The frequency with which to read the Modbus registers in msec 
    *   integer

[cabtr_te.iocsh](iocsh/cabtr_te.iocsh) sets up a sensor connected to the CABTR device

*   **DEVICENAME**
    *   ESS name of the sensor
    *   string
*   **CONTROLLER**
    *   The ESS name of the controller to which this sensor is connected to
    *   string
*   **CHANNEL**
    *   The channel on the controller to which this sensor is connected. Valid values are 1..8
    *   integer


### The python script

The python script should be run as
```
$ python3 sendfile.py <devicename> <filename> [<other filenames> ...]
```
There are some additional options, but these are the main ones.

When you run this, it will choose the files and send them, one at a time to the CABTR device. There is a
(default) 1s delay between sends, as it appears that it is possible to overwhelm the CABTR devices.

## Remarks

* It is not possible at the moment to use this module to delete any files. This should not be a problem, as there
  is room for several hundred coefficient files
* The CABTR devices are not very fast, and it seems quite easy to overwhelm them. It is probably best to send a
  single file at a time.
