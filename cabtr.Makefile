where_am_I := $(dir $(abspath $(lastword $(MAKEFILE_LIST))))
include $(E3_REQUIRE_TOOLS)/driver.makefile



############################################################################
#
# Add any required modules here that come from startup scripts, etc.
#
############################################################################

REQUIRED += modbus
ifneq ($(strip $(MODBUS_DEP_VERSION)),)
modbus_VERSION:=$(MODBUS_DEP_VERSION)
endif

REQUIRED += calc
ifneq ($(strip $(CALC_DEP_VERSION)),)
calc_VERSION:=$(CALC_DEP_VERSION)
endif

############################################################################
#
# If you want to exclude any architectures:
#
############################################################################



############################################################################
#
# If you want to allow specific architectures only:
#
############################################################################

EXCLUDE_ARCHS += linux-corei7-poky
EXCLUDE_ARCHS += linux-ppc64e6500


############################################################################
#
# Relevant directories to point to files
#
############################################################################

APP:=.
APPDB:=$(APP)/db
APPSRC:=$(APP)/src
APPSCRIPTS:=$(APP)/scripts
#APPCMDS:=$(APP)/cmds


############################################################################
#
# Add any files that should be copied to $(module)/Db
#
############################################################################

TEMPLATES += $(wildcard $(APPDB)/*.db)
TEMPLATES += $(wildcard $(APPDB)/*.template)
#TEMPLATES += $(wildcard $(APPDB)/*.substitutions)

# USR_INCLUDES += -I$(where_am_I)$(APPSRC)


############################################################################
#
# Add any files that need to be compiled (e.g. .c, .cpp, .st, .stt)
#
############################################################################

SOURCES   += $(APPSRC)/cabtr.c


LIB_SYS_LIBS += curl

############################################################################
#
# Add any .dbd files that should be included (e.g. from user-defined functions, etc.)
#
############################################################################

DBDS   += $(APPSRC)/cabtr.dbd


############################################################################
#
# Add any header files that should be included in the install (e.g. 
# StreamDevice or asyn header files that are used by other modules)
#
############################################################################

#HEADERS   += 


############################################################################
#
# Add any startup scripts that should be installed in the base directory
#
############################################################################

SCRIPTS += $(wildcard $(APPSCRIPTS)/*.py)
SCRIPTS += $(wildcard ../iocsh/*.iocsh)


############################################################################
#
# If you have any .substitution files, and template files, add them here.
#
############################################################################

SUBS=$(wildcard $(APPDB)/*.substitutions)
# TMPS=$(wildcard $(APPDB)/*.template)

USR_DBFLAGS += -I . -I ..
USR_DBFLAGS += -I $(EPICS_BASE)/db
USR_DBFLAGS += -I $(APPDB)


.PHONY: vlibs
vlibs:
