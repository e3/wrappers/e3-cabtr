require cabtr

epicsEnvSet(IPADDR, "localhost")
epicsEnvSet(CONTROLLER, "module-test:cabtr-controller-001")
epicsEnvSet(TE, "module-test:cabtr-te")
epicsEnvSet(POLL, "1000")

iocshLoad("$(cabtr_DIR)/cabtr_monitor.iocsh", "DEVICENAME=$(CONTROLLER), IPADDR=$(IPADDR), POLL=$(POLL), MB_PORT = 5020, WB_PORT = 8080")

iocshLoad("$(cabtr_DIR)/cabtr_te.iocsh" "DEVICENAME=$(TE)-001, CONTROLLER=$(CONTROLLER), CHANNEL=1")
iocshLoad("$(cabtr_DIR)/cabtr_te.iocsh" "DEVICENAME=$(TE)-002, CONTROLLER=$(CONTROLLER), CHANNEL=2")
iocshLoad("$(cabtr_DIR)/cabtr_te.iocsh" "DEVICENAME=$(TE)-003, CONTROLLER=$(CONTROLLER), CHANNEL=3")
iocshLoad("$(cabtr_DIR)/cabtr_te.iocsh" "DEVICENAME=$(TE)-004, CONTROLLER=$(CONTROLLER), CHANNEL=4")
iocshLoad("$(cabtr_DIR)/cabtr_te.iocsh" "DEVICENAME=$(TE)-005, CONTROLLER=$(CONTROLLER), CHANNEL=5")
iocshLoad("$(cabtr_DIR)/cabtr_te.iocsh" "DEVICENAME=$(TE)-006, CONTROLLER=$(CONTROLLER), CHANNEL=6")
iocshLoad("$(cabtr_DIR)/cabtr_te.iocsh" "DEVICENAME=$(TE)-007, CONTROLLER=$(CONTROLLER), CHANNEL=7")
iocshLoad("$(cabtr_DIR)/cabtr_te.iocsh" "DEVICENAME=$(TE)-008, CONTROLLER=$(CONTROLLER), CHANNEL=8")

#iocshLoad("$(cabtr_DIR)/cabtr_send.iocsh", "IOCNAME=$(DEVICENAME), CABTR_HOST_URL=$(IPADDR)")
