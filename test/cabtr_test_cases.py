import epics
from run_iocsh import IOC
import os
import pytest
import sys
import time


CABTR_SIMULATOR_DIR = "cabtr/scripts"
CABTR_INSTALLED_CALIBRATION_FILES = [
    "pt100.pol",
    "pt101.pol",
]
CABTR_CHANNEL_NAMES = [
    "1one1",
    "2two2",
    "3three3",
    "4four4",
    "5five5",
    "6six6",
    "7seven7",
    "8eight8",
]
CABTR_CHANNEL_CURVES = [
    "curve_for_ch1.pol",
    "curve_for_ch2.cof",
    "curve_for_ch3.pol",
    "curve_for_ch4.cof",
    "curve_for_ch5.pol",
    "curve_for_ch6.cof",
    "curve_for_ch7.pol",
    "curve_for_ch8.cof",
]
CABTR_TIME_LIMIT = 60


class CabtrTestHarness(object):
    def __init__(self):

        # Get values from the environment
        self.EPICS_BASE = os.environ.get("EPICS_BASE")
        self.REQUIRE_VERSION = os.environ.get("E3_REQUIRE_VERSION")
        self.MOD_VERSION = os.environ.get("E3_MODULE_VERSION")
        if self.MOD_VERSION is not None:
            self.MOD_VERSION = f",{self.MOD_VERSION}"
        self.TEMP_CELL_PATH = os.environ.get("TEMP_CELL_PATH")
        if self.TEMP_CELL_PATH is None:
            self.TEMP_CELL_PATH = "cellMods"

        # run-iocsh parameters
        self.IOCSH_PATH = (
            f"{self.EPICS_BASE}/require/{self.REQUIRE_VERSION}/bin/iocsh"
        )

        self.TestArgs = [
            "-l",
            self.TEMP_CELL_PATH,
            "-r",
            f"cabtr{self.MOD_VERSION}",
        ]

        self.controller_prefix = "module-test:cabtr-controller-001"
        self.te_prefix = "module-test:cabtr-te-"

        self.cmd = "test/cmds/st.cmd"

        # Simulator
        sys.path.append(os.path.abspath(CABTR_SIMULATOR_DIR))
        from cabtr_sim import Cabtr as CabtrSim
        self.CabtrSim = CabtrSim

        self.simulator = None
        self.simulatorModbusPort = 5020
        self.simulatorHTTPPort = 8080
        self.simluatorURI = "localhost.localdomain"


    def __enter__(self):
        return self


    def __exit__(self, type, value, traceback):
        self.cleanup()


    def create_ioc(self):
        return IOC(
            *self.TestArgs,
            self.cmd,
            ioc_executable=self.IOCSH_PATH,
        )


    def cleanup(self):
        # Stop simulator
        self.stop_simulator()

        # Clear pyepics cache
        # Has to be done so that PVs of previous IOCs won't be returned for new IOCs
        epics.ca.clear_cache()


    def start_simulator(self, **kwargs):
        if self.simulator:
            return

        self.simulator = self.CabtrSim(mb_port = self.simulatorModbusPort, web_port = self.simulatorHTTPPort, verbose = False, **kwargs)
        self.simulator.start()


    def stop_simulator(self):
        if self.simulator:
            self.simulator.stop()
            assert not self.simulator.is_running()
        self.simulator = None


    def is_simulator_running(self):
        return self.simulator and self.simulator.is_running()


# Standard test fixture
@pytest.fixture(scope="function")
def test_inst():
    """
    Instantiate test harness, start the simulator,
    yield the harness handle to the test,
    stop the simulator on test end / failure
    """
    # Create handle to Test Harness
    with CabtrTestHarness() as test_inst:
        # Drop to test
        yield test_inst
    # Check if simulator is stopped
    assert not test_inst.is_simulator_running()


class TestCalibration(object):
    def test_get_uploaded_calibration_files(self, test_inst):
        test_inst.start_simulator(installed_calibration_files = CABTR_INSTALLED_CALIBRATION_FILES)
        assert test_inst.is_simulator_running()

        with test_inst.create_ioc() as ioc:
            assert ioc.is_running()

            # The IOC should already be updating calibration file list

            http_response_pv = epics.get_pv(f"{test_inst.controller_prefix}:CalibRereadHttpCode", auto_monitor = True, connect = True)
            reread_pv = epics.get_pv(f"{test_inst.controller_prefix}:#CalibReread", auto_monitor = True, connect = True)

            start = time.time()
            while http_response_pv.status == 17 or reread_pv.status == 17:
                epics.poll(0.5)
                assert http_response_pv.connected
                assert reread_pv.connected
                # For some reason get_ctrlvars() needs to be called otherwise reread_pv.status is not updated
                reread_pv.get_ctrlvars()
                assert time.time() - start < CABTR_TIME_LIMIT

            assert reread_pv.status == 0
            assert reread_pv.severity == epics.NO_ALARM

            assert http_response_pv.status == 0
            assert int(http_response_pv.value) == 200

            calibfiles_pv = epics.get_pv(f"{test_inst.controller_prefix}:CalibFiles-RB", connect = True)
            calibfiles_pv.get_ctrlvars()
            assert calibfiles_pv.status == 0
            assert calibfiles_pv.count == len(CABTR_INSTALLED_CALIBRATION_FILES)
            assert calibfiles_pv.value == CABTR_INSTALLED_CALIBRATION_FILES


    def test_get_channel_names(self, test_inst):
        test_inst.start_simulator(channel_names = CABTR_CHANNEL_NAMES, channel_curves = CABTR_CHANNEL_CURVES)
        assert test_inst.is_simulator_running()

        with test_inst.create_ioc() as ioc:
            assert ioc.is_running()

            # The IOC should already be updating channel information

            http_response_pv = epics.get_pv(f"{test_inst.controller_prefix}:ChannelRereadHttpCode", auto_monitor = True, connect = True)
            reread_pv = epics.get_pv(f"{test_inst.controller_prefix}:#ChannelReread", auto_monitor = True, connect = True)

            start = time.time()
            while http_response_pv.status == 17 or reread_pv.status == 17:
                epics.poll(0.5)
                # For some reason get_ctrlvars() needs to be called otherwise reread_pv.status is not updated
                reread_pv.get_ctrlvars()
                assert time.time() - start < CABTR_TIME_LIMIT

            assert reread_pv.status == 0
            assert reread_pv.severity == epics.NO_ALARM

            assert http_response_pv.status == 0
            assert int(http_response_pv.value) == 200

            channelnames_pv = epics.get_pv(f"{test_inst.controller_prefix}:ChannelNames-RB", connect = True)
            channelnames_pv.get_ctrlvars()
            assert channelnames_pv.status == 0
            assert channelnames_pv.count == len(CABTR_CHANNEL_NAMES)
            assert channelnames_pv.value == CABTR_CHANNEL_NAMES

            channelcurves_pv = epics.get_pv(f"{test_inst.controller_prefix}:ChannelCurves-RB", connect = True)
            channelcurves_pv.get_ctrlvars()
            assert channelcurves_pv.status == 0
            assert channelcurves_pv.count == len(CABTR_CHANNEL_CURVES)
            assert channelcurves_pv.value == CABTR_CHANNEL_CURVES


    def test_upload_calibration_file_with_sendfile_script(self, test_inst):
        sys.path.append(os.path.abspath("."))
        from sendfile import main as sendfile_main
        test_inst.start_simulator(installed_calibration_files = CABTR_INSTALLED_CALIBRATION_FILES)
        assert test_inst.is_simulator_running()

        with test_inst.create_ioc() as ioc:
            assert ioc.is_running()

            upload_pv = epics.get_pv(f"{test_inst.controller_prefix}:#UploadCalibFile", auto_monitor = True, connect = True)
            # Should be undefined
            assert upload_pv.status == 17
            assert upload_pv.severity == epics.INVALID_ALARM

            http_response_pv = epics.get_pv(f"{test_inst.controller_prefix}:UploadCalibFileHttpCode", auto_monitor = True, connect = True)
            # Should be undefined
            assert http_response_pv.status == 17
            assert http_response_pv.severity == epics.INVALID_ALARM

            # Check reread PVs early on
            reread_http_response_pv = epics.get_pv(f"{test_inst.controller_prefix}:CalibRereadHttpCode", auto_monitor = True, connect = True)
            calibfiles_pv = epics.get_pv(f"{test_inst.controller_prefix}:CalibFiles-RB", auto_monitor = True, connect = True)

            # Wait until the automatic update is done
            start = time.time()
            while reread_http_response_pv.status == 17 or calibfiles_pv.status == 17:
                epics.poll(0.5)
                # For some reason get_ctrlvars() needs to be called otherwise reread_http_response_pv.status is not updated
                reread_http_response_pv.get_ctrlvars()
                assert time.time() - start < CABTR_TIME_LIMIT

            calibfiles_pv.get_ctrlvars()
            calibfiles_time = calibfiles_pv.timestamp

            calibration_filename = "sendfile.py"

            sendfile_main([test_inst.controller_prefix, os.path.abspath(calibration_filename)])
            with open(os.path.abspath(calibration_filename), "rt") as f:
                calibration_data = f.read()

            start = time.time()
            while http_response_pv.status == 17 or upload_pv.status == 17:
                epics.poll(0.5)
                # For some reason get_ctrlvars() needs to be called otherwise upload_pv.status is not updated
                upload_pv.get_ctrlvars()
                assert time.time() - start < CABTR_TIME_LIMIT

            assert http_response_pv.status == 0
            assert int(http_response_pv.value) == 200

            upload_reply_pv = epics.get_pv(f"{test_inst.controller_prefix}:UploadCalibFileReply", connect = True)
            upload_reply_pv.get_ctrlvars()
            assert upload_reply_pv.status == 0
            assert upload_reply_pv.count == len(calibration_data)
            assert bytes(list(upload_reply_pv.value)).decode() == calibration_data

            # Reread should have been triggered by the upload
            start = time.time()
            while calibfiles_pv.timestamp == calibfiles_time:
                epics.poll(0.5)
                # For some reason get_ctrlvars() needs to be called otherwise calibfiles_pv.status is not updated
                calibfiles_pv.get_ctrlvars()
                assert time.time() - start < CABTR_TIME_LIMIT

            assert reread_http_response_pv.status == 0
            assert int(reread_http_response_pv.value) == 200

            calibfiles_pv.get_ctrlvars()
            assert calibfiles_pv.status == 0
            assert calibfiles_pv.count == len(CABTR_INSTALLED_CALIBRATION_FILES) + 1
            assert calibration_filename in calibfiles_pv.value
