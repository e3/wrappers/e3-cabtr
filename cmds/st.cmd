require cabtr

# An example startup script for a CABTR device with 8 sensors

epicsEnvSet(DEVICENAME, "TS2-010CRM:Cryo-TC-003")
epicsEnvSet(IPADDR, "ts2-cabtr-03.tn.esss.lu.se")
epicsEnvSet(CONTROLLER, "TS2-010CRM:Cryo-TC-003")
epicsEnvSet(POLL, "1000")

iocshLoad("$(cabtr_DIR)/cabtr_monitor.iocsh", "DEVICENAME=$(DEVICENAME), IPADDR=$(IPADDR), POLL=$(POLL)")

iocshLoad("$(cabtr_DIR)/cabtr_te.iocsh" "DEVICENAME=TS2-010CRM:Cryo-TE-021, CONTROLLER=$(CONTROLLER), CHANNEL=7")
iocshLoad("$(cabtr_DIR)/cabtr_te.iocsh" "DEVICENAME=TS2-010CRM:Cryo-TE-033, CONTROLLER=$(CONTROLLER), CHANNEL=1")
iocshLoad("$(cabtr_DIR)/cabtr_te.iocsh" "DEVICENAME=TS2-010CRM:Cryo-TE-034, CONTROLLER=$(CONTROLLER), CHANNEL=2")
iocshLoad("$(cabtr_DIR)/cabtr_te.iocsh" "DEVICENAME=TS2-010CRM:Cryo-TE-042, CONTROLLER=$(CONTROLLER), CHANNEL=5")
iocshLoad("$(cabtr_DIR)/cabtr_te.iocsh" "DEVICENAME=TS2-010CRM:Cryo-TE-043, CONTROLLER=$(CONTROLLER), CHANNEL=3")
iocshLoad("$(cabtr_DIR)/cabtr_te.iocsh" "DEVICENAME=TS2-010CRM:Cryo-TE-044, CONTROLLER=$(CONTROLLER), CHANNEL=4")
iocshLoad("$(cabtr_DIR)/cabtr_te.iocsh" "DEVICENAME=TS2-010CRM:Cryo-TE-048, CONTROLLER=$(CONTROLLER), CHANNEL=6")
iocshLoad("$(cabtr_DIR)/cabtr_te.iocsh" "DEVICENAME=TS2-010CRM:Cryo-TE-057, CONTROLLER=$(CONTROLLER), CHANNEL=8")
