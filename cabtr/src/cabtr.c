#include <callback.h>
#include <dbAccessDefs.h>
#include <dbFldTypes.h>
#include <aSubRecord.h>
#include <registryFunction.h>
#include <epicsExport.h>
#include <errlog.h>
#include <epicsStdio.h>
#include <epicsString.h>
#include <epicsThread.h>
#include <ctype.h>
#include <stdlib.h>
#include <string.h>
#include <iocsh.h>
#include <curl/curl.h>

#define NUM_CHANNELS 8

int cabtrDebug;

typedef struct response_buffer
{
    char *buffer;
    size_t buffer_size;
    size_t offset;
} response_buffer;


typedef struct aSub_private
{
    CALLBACK callback;
    char user_agent[256];
    CURL *curl;
    char *buffer;
    size_t buffer_size;
    int response_code;
} aSub_private;


static aSub_private* create_aSub_private(size_t buffer_size);
static void cleanupCurl(aSub_private *as_priv);
static size_t write_data(void *buffer, size_t size, size_t nmemb, void *userp);
static void getUserAgent(char *buffer);


static aSub_private* create_aSub_private(size_t buffer_size) {
    aSub_private *as_priv = (aSub_private *)calloc(1, sizeof(aSub_private));

    /* Allow using a preallocated buffer */
    if (buffer_size) {
        as_priv->buffer = (char *)calloc(1, buffer_size);
        as_priv->buffer_size = buffer_size;
    }

    getUserAgent(as_priv->user_agent);

    return as_priv;
}


static void cleanupCurl(aSub_private *as_priv) {
    curl_easy_cleanup(as_priv->curl);
    as_priv->curl = NULL;
}


static size_t write_data(void *buffer, size_t size, size_t nmemb, void *userp) {
    response_buffer *prb = (response_buffer *)userp;
    if (cabtrDebug) {
        printf("write_data called:\n");
        printf("  size:   %zu\n", size);
        printf("  nmemb:  %zu\n", nmemb);
    }

    size_t total = size * nmemb;
    if (prb->offset + total >= prb->buffer_size) {
        errlogSevPrintf(errlogFatal, "Response too long\n");
        return 0;
    }

    strncpy(prb->buffer + prb->offset, buffer, total);
    prb->offset += total;
    prb->buffer[prb->offset] = '\0';
    return total;
}

static void getUserAgent(char *buffer) {
    const char *ioc_name = getenv("IOCNAME");
    const char *base_version = strstr(getenv("EPICS_BASE"),"base-");

    if (base_version) {
        if (ioc_name && ioc_name[0]) {
            sprintf(buffer, "%s (EPICS BASE %s IOC)", ioc_name, base_version);
        } else {
            sprintf(buffer, "EPICS BASE %s IOC", base_version);
        }
    } else {
        if (ioc_name && ioc_name[0]) {
            sprintf(buffer, "%s (EPICS IOC)", ioc_name);
        } else {
            sprintf(buffer, "EPICS IOC");
        }
    }
}

static int initCurl(aSub_private *as_priv, const char* url) {
    as_priv->curl = curl_easy_init();
    if (!as_priv->curl)
        return -1;
    curl_easy_setopt(as_priv->curl, CURLOPT_VERBOSE, cabtrDebug > 1);
    curl_easy_setopt(as_priv->curl, CURLOPT_URL, url);
    curl_easy_setopt(as_priv->curl, CURLOPT_NOSIGNAL, 1);
    curl_easy_setopt(as_priv->curl, CURLOPT_WRITEFUNCTION, write_data);
    curl_easy_setopt(as_priv->curl, CURLOPT_USERAGENT, as_priv->user_agent);

    return 0;
}

/*
 * Set `dest` to the desired URL: http://prec->a:prec->b/path
 */
static int setUrl(aSubRecord *prec, char *dest, size_t destlen, const char *path) {

    if (prec->fta != DBF_STRING) {
        errlogSevPrintf(errlogFatal, "FTA must be 'STRING'\n");
        return 1;
    }

    if (prec->ftb != DBF_LONG) {
        errlogSevPrintf(errlogFatal, "FTB must be 'LONG'\n");
        return 1;
    }

    char *hostinfo = prec->a;
    if (hostinfo == NULL || hostinfo[0] == '\0') {
        errlogPrintf("URL not set\n");
        return 1;
    }

    char *colon = strrchr(hostinfo, ':');
    if (colon)
        colon[0] = '\0';

    if (snprintf(dest, destlen, "http://%s:%d/%s", hostinfo, *(int*)prec->b, path) >= destlen) {
        errlogSevPrintf(errlogFatal, "URL too long\n");
        return 1;
    }

    if (cabtrDebug)
        printf("URL: %s\n", dest);

    return 0;
}

static int sendFile(CURL *c, const char *filename, const char *data, char *buffer, size_t rb_size, int *response_code) {
    struct curl_httppost *post=NULL;
    struct curl_httppost *last=NULL;
    CURLcode res;
    response_buffer b = {buffer, rb_size};

    curl_easy_setopt(c, CURLOPT_WRITEDATA, &b);

    curl_formadd(&post, &last, CURLFORM_COPYNAME, "type", CURLFORM_COPYCONTENTS, "upload", CURLFORM_END);
    curl_formadd(&post, &last, CURLFORM_COPYNAME, "binary", CURLFORM_FILENAME, filename,
       CURLFORM_CONTENTTYPE, "application/octet-stream",
       CURLFORM_BUFFERPTR,
       data, CURLFORM_END);

    curl_easy_setopt(c, CURLOPT_HTTPPOST, post);
    res = curl_easy_perform(c);

    /* cURL uses long but EPICS uses int (even with DBF_LONG) */
    long long_response_code;
    curl_easy_getinfo(c, CURLINFO_RESPONSE_CODE, &long_response_code);
    *response_code = (int) long_response_code;

    if (cabtrDebug) {
        printf("Result: %s\n", curl_easy_strerror(res));
        printf("HTTP response code: %d\n", *response_code);
    }

    curl_formfree(post);
    return *response_code;
}

static void sendFileThread(void *param) {
    aSubRecord *prec = (aSubRecord *)param;
    aSub_private *as_priv = (aSub_private *)prec->dpvt;

    sendFile(as_priv->curl, prec->c, prec->d, prec->vala, prec->nova, &as_priv->response_code);

    while (callbackRequestProcessCallback(&as_priv->callback, priorityLow, prec) == S_db_bufFull) {
        epicsThreadSleep(1.0);
    }
}

static int httpGet(CURL *c, char *buffer, size_t rb_size, int *response_code) {
    CURLcode res;
    response_buffer b = {buffer, rb_size};

    curl_easy_setopt(c, CURLOPT_WRITEDATA, &b);

    curl_easy_setopt(c, CURLOPT_HTTPGET, 1);
    res = curl_easy_perform(c);

    /* cURL uses long but EPICS uses int (even with DBF_LONG) */
    long long_response_code;
    curl_easy_getinfo(c, CURLINFO_RESPONSE_CODE, &long_response_code);
    *response_code = (int) long_response_code;

    if (cabtrDebug) {
        printf("Result: %s\n", curl_easy_strerror(res));
        printf("HTTP response code: %d\n", *response_code);
    }

    return *response_code;
}

static void httpGetThread(void *param) {
    aSubRecord *prec = (aSubRecord *)param;
    aSub_private *as_priv = (aSub_private *)prec->dpvt;

    httpGet(as_priv->curl, as_priv->buffer, as_priv->buffer_size, &as_priv->response_code);

    while (callbackRequestProcessCallback(&as_priv->callback, priorityLow, prec) == S_db_bufFull) {
        epicsThreadSleep(1.0);
    }
}

static long subSendFileInit(aSubRecord *prec) {
    int err = 0;

    if (prec->ftvb != DBF_LONG) {
        errlogSevPrintf(errlogFatal, "FTVB must be 'LONG'\n");
        err = -1;
    }

    if (err == 0 && prec->dpvt == NULL) {
        prec->dpvt = create_aSub_private(0);
    }

    return err;
}

static long subSendFileProcRequest(aSubRecord *prec);
static long subSendFileProcResponse(aSubRecord *prec);

static long subSendFileProc(aSubRecord *prec) {
    if (prec->pact == FALSE)
        return subSendFileProcRequest(prec);

    return subSendFileProcResponse(prec);
}

static long subSendFileProcRequest(aSubRecord *prec) {
    if (subSendFileInit(prec))
        return -1;

    char url[128];
    if (setUrl(prec, url, sizeof(url), "index.html"))
        return -1;

    aSub_private *as_priv = (aSub_private *)prec->dpvt;

    if (initCurl(as_priv, url)) {
        errlogPrintf("initCurl failed\n");
        return -1;
    }

    ((char*)prec->vala)[0] = '\0';

    prec->pact = TRUE;

    if (epicsThreadCreate("subSendFileProc", epicsThreadPriorityMedium, epicsThreadStackMedium, sendFileThread, prec)) {
        return 0;
    }

    errlogSevPrintf(errlogFatal, "Cannot start thread to upload calibration file\n");

    prec->pact = FALSE;

    cleanupCurl(as_priv);

    return -1;
}

static long subSendFileProcResponse(aSubRecord *prec) {
    aSub_private *as_priv = (aSub_private *)prec->dpvt;

    cleanupCurl(as_priv);

    *(int *)prec->valb = as_priv->response_code;

    prec->neva = strlen(prec->vala);

    if (as_priv->response_code != 200)
        return -1;

    return 0;
}

/*
 * Get Curve File list from CABTR
 */
static long subGetFileListInit(aSubRecord *prec) {
    int err = 0;

    if (prec->ftva != DBF_STRING) {
        errlogSevPrintf(errlogFatal, "FTVA must be 'STRING'\n");
        err = -1;
    }

    if (prec->ftvb != DBF_LONG) {
        errlogSevPrintf(errlogFatal, "FTVB must be 'LONG'\n");
        err = -1;
    }

    if (err == 0 && prec->dpvt == NULL) {
        /* CABTR can hold 500 calibration files. Arbitrary upper limit on reponse line length is 100 characters */
        const size_t DIR_DUMP_RESPONSE_SIZE = 500 * 100;
        prec->dpvt = create_aSub_private(DIR_DUMP_RESPONSE_SIZE);
    }

    return err;
}

static long subGetFileListProcRequest(aSubRecord *prec);
static long subGetFileListProcResponse(aSubRecord *prec);

static long subGetFileListProc(aSubRecord *prec) {
    if (prec->pact == FALSE)
        return subGetFileListProcRequest(prec);

    return subGetFileListProcResponse(prec);
}

static long subGetFileListProcRequest(aSubRecord *prec) {
    if (subGetFileListInit(prec))
       return -1;

    char url[128];
    if (setUrl(prec, url, sizeof(url), "lin/dump_dir.txt"))
        return -1;

    aSub_private *as_priv = (aSub_private *)prec->dpvt;

    if (initCurl(as_priv, url)) {
        errlogPrintf("initCurl failed\n");
        return -1;
    }

    prec->pact = TRUE;

    if (epicsThreadCreate("subGetFileListProc", epicsThreadPriorityMedium, epicsThreadStackMedium, httpGetThread, prec)) {
        return 0;
    }

    errlogSevPrintf(errlogFatal, "Cannot start thread to retrieve calibration file list\n");

    prec->pact = FALSE;

    cleanupCurl(as_priv);

    return -1;
}


static long subGetFileListProcResponse(aSubRecord *prec) {
    aSub_private *as_priv = (aSub_private *)prec->dpvt;

    cleanupCurl(as_priv);

    *(int *)prec->valb = as_priv->response_code;

    if (as_priv->response_code != 200)
        return -1;

    char *response = as_priv->buffer;

    if (cabtrDebug)
        printf("Response: |%s|\n", response);

    /* dump_dir.txt looks like this:

pt100.pol 956
TT07X107597.cof 1678
TT08X108205.cof 1678
TT09X108206.cof 1678
[USED (ko)] : 5652 
[FREE (ko)] : 638 
   * I don't really know what the numbers mean (filesize perhaps?) but it does not really matter.
   * I also don't know what the last two lines mean but they are ignored because of the extra space(s)
   */

    /* Iterate over the lines of the response
     * This implementation ignores the last line if not ends with newline
     */
    char *resp;
    char *eol;
    int idx = 0;
    size_t string_len = dbValueSize(DBF_STRING);
    for (resp = response; (eol = strpbrk(resp, "\r\n")); resp = eol + 1) {
        /* Skip empty lines */
        if (resp == eol)
            continue;

        eol[0] = '\0';

        if (cabtrDebug)
            printf("Line: |%s|\n", resp);

        /* Split using space as delimiter */
        char *space = strchr(resp, ' ');
        /* Skip lines without a space */
        if (space == NULL)
            continue;

        space[0] = '\0';

        /* Ignore lines with more than one space */
        if (strchr(space + 1, ' '))
            continue;

        if (strlen(resp) + 1 >= string_len) {
            errlogPrintf("Ignoring too long filename: %s\n", resp);
            continue;
        }

        if (cabtrDebug)
            printf("filename: |%s|\n", resp);

        strcpy((char*)(prec->vala) + string_len * idx++, resp);
        prec->neva = idx;
    }

    return 0;
}

/*
 * Get channel configuration from CABTR
 */
static long subGetChannelCfgInit(aSubRecord *prec) {
    int err = 0;

    if (prec->ftva != DBF_STRING) {
        errlogSevPrintf(errlogFatal, "FTVA must be 'STRING'\n");
        err = -1;
    }

    if (prec->ftvc != DBF_LONG) {
        errlogSevPrintf(errlogFatal, "FTVC must be 'LONG'\n");
        err = -1;
    }

    if (prec->nova != NUM_CHANNELS || prec->novb != NUM_CHANNELS) {
        errlogSevPrintf(errlogFatal, "NOVA and NOVB must be %d\n", NUM_CHANNELS);
        err = -1;
    }

    if (err == 0 && prec->dpvt == NULL) {
        /* Arbitrary maximum response size; 50 lines, 100 character long lines */
        const size_t CONFIG_INIT_RESPONSE_SIZE = 50 * 100;
        prec->dpvt = create_aSub_private(CONFIG_INIT_RESPONSE_SIZE);
    }

    return err;
}

static long subGetChannelCfgProcRequest(aSubRecord *prec);
static long subGetChannelCfgProcResponse(aSubRecord *prec);

static long subGetChannelCfgProc(aSubRecord *prec) {
    if (prec->pact == FALSE)
        return subGetChannelCfgProcRequest(prec);

    return subGetChannelCfgProcResponse(prec);
}


static long subGetChannelCfgProcRequest(aSubRecord *prec) {
    if (subGetChannelCfgInit(prec))
        return -1;

    char url[128];
    if (setUrl(prec, url, sizeof(url), "config/init.txt"))
        return -1;

    aSub_private *as_priv = (aSub_private *)prec->dpvt;

    if (initCurl(as_priv, url)) {
        errlogPrintf("initCurl failed\n");
        return -1;
    }

    size_t string_len = dbValueSize(DBF_STRING);

    /* Initialize output arrays */
    prec->neva = NUM_CHANNELS;
    memset(prec->vala, 0, prec->neva * string_len);
    prec->nevb = NUM_CHANNELS;
    memset(prec->valb, 0, prec->nevb * string_len);

    prec->pact = TRUE;

    if (epicsThreadCreate("subGetChannelCfgProc", epicsThreadPriorityMedium, epicsThreadStackMedium, httpGetThread, prec)) {
        return 0;
    }

    errlogSevPrintf(errlogFatal, "Cannot start thread to retrieve channel info\n");

    prec->pact = FALSE;

    cleanupCurl(as_priv);

    return -1;
}

static long subGetChannelCfgProcResponse(aSubRecord *prec) {
    aSub_private *as_priv = (aSub_private *)prec->dpvt;

    cleanupCurl(as_priv);

    *(int *)prec->valc = as_priv->response_code;

    if (as_priv->response_code != 200)
        return -1;

    char *response = as_priv->buffer;

    /* init.txt looks like this:

Nom voie1 :     TT64
Fichier voie1 :    pt100.pol
Nom voie2 :     TT24
Fichier voie2 :    pt100.pol
Nom voie3 :     TT13
Fichier voie3 :    pt100.pol
Nom voie4 :     TT41
Fichier voie4 :    pt100.pol
Nom voie5 :     TT33
Fichier voie5 :    pt100.pol
Nom voie6 :     TT34
Fichier voie6 :    pt100.pol
Nom voie7 :     TT43
Fichier voie7 :    pt100.pol
Nom voie8 :     TT44
Fichier voie8 :    pt100.pol

Adresse IP : 172.16.110.52
Masque de sous reseau : 255.255.254.0
Passerelle : 172.16.110.254

Profinet : ON
Madapt : 6
Alarm_Temp : 55.0
   * Nom voie is the name of the channel
   * Fichier voie is the configured calibration file
   */

    if (cabtrDebug)
        printf("Response: |%s|\n", response);

    /* Iterate over the lines of the response
     * This implementation ignores the last line if not ends with newline
     */
    char *resp;
    char *eol;
    int idx = 0;
    const char channel_name_debug[] = "Channel name";
    const char channel_curve_debug[] = "Channel curve file";
    const char channel_name_header[] = "Nom voie";
    const char channel_curve_header[] = "Fichier voie";
    size_t channel_name_header_len = strlen(channel_name_header);
    size_t channel_curve_header_len = strlen(channel_curve_header);
    size_t string_len = dbValueSize(DBF_STRING);
    for (resp = response; (eol = strpbrk(resp, "\r\n")); resp = eol + 1) {
        /* Skip empty lines */
        if (resp == eol)
            continue;

        eol[0] = '\0';

        if (cabtrDebug)
            printf("Line: |%s|\n", resp);

        /* Split using : as delimiter */
        char *colon = strchr(resp, ':');
        /* Skip lines without a colon */
        if (colon == NULL)
            continue;

        colon[0] = '\0';

        /* Parse channel name and curve name headers */
        char *valx;
        char const *entry_debug;
        if (strstr(resp, channel_name_header) == resp) {
            valx = (char*)(prec->vala);
            entry_debug = channel_name_debug;
            /* Get number after string zero based */
            idx = resp[channel_name_header_len] - '1';
        } else if (strstr(resp, channel_curve_header) == resp) {
            valx = (char*)(prec->valb);
            entry_debug = channel_curve_debug;
            /* Get number after string zero based */
            idx = resp[channel_curve_header_len] - '1';
        } else {
            /* Don't care */
            continue;
        }

        if (idx < 0 || idx >= NUM_CHANNELS) {
            errlogPrintf("Channel number out of range: %d\n", idx + 1);
            continue;
        }

        /* Strip leading whitespace */
        for (resp = colon + 1; isspace(resp[0]); ++resp) ;

        if (strlen(resp) + 1 >= string_len) {
            errlogPrintf("Ignoring too long entry: %s\n", resp);
            continue;
        }

        if (cabtrDebug)
            printf("%s: |%s|\n", entry_debug, resp);

        strcpy(valx + string_len * idx, resp);
    }

    return 0;
}

epicsRegisterFunction(subSendFileInit);
epicsRegisterFunction(subSendFileProc);
epicsRegisterFunction(subGetFileListInit);
epicsRegisterFunction(subGetFileListProc);
epicsRegisterFunction(subGetChannelCfgInit);
epicsRegisterFunction(subGetChannelCfgProc);
epicsExportAddress(int, cabtrDebug);
