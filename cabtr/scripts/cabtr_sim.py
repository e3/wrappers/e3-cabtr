#!/usr/bin/env python3

from __future__ import absolute_import
from __future__ import print_function

from time import sleep
from threading import Thread

import pymodbus.server.sync as modbus
from pymodbus.datastore.store import ModbusSequentialDataBlock
from pymodbus.datastore.context import ModbusServerContext, ModbusSlaveContext

from http import HTTPStatus as HTTPStatus
from http.server import SimpleHTTPRequestHandler as SimpleHTTPRequestHandler
from http.server import HTTPServer as HTTPServer
from http.client import HTTPMessage as HTTPMessage

from cgi import FieldStorage as FieldStorage


class CabtrHTTPRequestHandler(SimpleHTTPRequestHandler):
    protocol_version = "HTTP/1.1"

    def do_GET(self):
        if self.path.endswith("/lin/dump_dir.txt"):
            resp = self.__dump_dir_txt()
        elif self.path.endswith("/config/init.txt"):
            resp = self.__init_txt()
        else:
            raise RuntimeError("Unknown GET path: " + self.path)

        self.__write(resp)


    def do_POST(self):
        if self.path.endswith("/index.html"):
            resp = self.__receive_calib_file()
        else:
            raise RuntimeError("Unknown POST path: " + self.path)

        self.__write(resp)


    def log_request(self, code):
        pass


    def __write(self, response):
        if isinstance(response, HTTPStatus):
            self.send_response(response)
        else:
            self.send_response(HTTPStatus.OK)
            self.send_header("Content-type", "text/html; charset=UTF-8;")
            self.send_header("Content-Length", len(response))
            self.send_header("Last-Modified", self.date_time_string())
            self.end_headers()
            self.wfile.write(response.encode() if isinstance(response, str) else response)
        self.wfile.flush()


    def __init_txt(self):
        dd = []
        for ch in self.server.cabtr_channels():
            dd.append("Nom voie{} : {}".format(ch._idx, ch.name))
            dd.append("Fichier voie{} : {}".format(ch._idx, ch.curve))


        dd.extend("""Adresse IP : 172.16.110.52
Masque de sous reseau : 255.255.254.0
Passerelle : 172.16.110.254

Profinet : ON
Madapt : 6
Alarm_Temp : 55.0

""".splitlines())

        return """
""".join(dd)


    def __dump_dir_txt(self):
        dd = self.server.installed_calibration_files()

        dd = [f"{filename} 420" for filename in dd]

        dd.append("[USED (ko)] : 690")
        dd.append("[FREE (ko)] : 690")
        dd.append("")

        return """
""".join(dd)


    def __receive_calib_file(self):
        form = FieldStorage(fp = self.rfile, headers = self.headers, environ = {'REQUEST_METHOD':'POST'})
        if form.getvalue("type") != "upload" or "binary" not in form or form["binary"].filename is None:
            return HTTPStatus.BAD_REQUEST

        calib_file = form["binary"]
        self.server.upload_calibration_file(calib_file.filename)

        return calib_file.value


class Channel(object):
    def __init__(self, mb, idx, name, curve):
        self.__mb      = mb
        self._idx      = idx
        self.__address = 304 + 30 * (idx - 1)
        self.name      = name
        self.curve     = curve
        self.__prog    = 0

        self.__name_a  = self.__address + 0
        self.__curve_a = self.__address + 8
        self.__prog_a  = self.__address + 24

        self.mb_name  = self.name
        self.mb_curve = self.curve


    def __str__(self):
        return """Channel{idx} // {addr}
\tName : {n} ({mn})
\tCurve: {c} ({mc})
\tProg : {p}
""".format(idx = self._idx, n = self.name, mn = self.mb_name, c = self.curve, mc = self.mb_curve, p = self.__prog, addr = self.__address)


    def is_this(self, address):
        if self.__address <= address and address < self.__address + 30:
            return True

        return False


    def commit(self):
        if self.__prog & 1:
            self.name   = self.mb_name
            pass
        if self.__prog & 2:
            self.curve  = self.mb_curve
            pass
        if self.__prog:
            self.__prog = 0
            print(self)
            self.serialize()


    def deserialize(self, address, values):
        if not self.is_this(address):
            print("Address does not belong to channel")
            raise RuntimeError("Address does not belong to channel")

        if address == self.__name_a:
            self.mb_name = Cabtr.deserialize(values, str)
        elif address == self.__curve_a:
            self.mb_curve = Cabtr.deserialize(values, str)
        elif address == self.__prog_a:
            self.__prog = Cabtr.deserialize(values, int)
        else:
            print("Not writeable address", address)
            raise RuntimeError("Not writeable address", address)

        # Commit our state
        self.serialize()
        print(self)


    def serialize(self):
        self.__mb.direct_setValues(self.__name_a,  Cabtr.serialize(self.mb_name, str, 8))
        self.__mb.direct_setValues(self.__curve_a, Cabtr.serialize(self.mb_curve, str, 16))
        self.__mb.direct_setValues(self.__prog_a,  Cabtr.serialize(self.__prog, int, 1))



class Cabtr(ModbusSequentialDataBlock):
    COMMIT_CHANNELS = 57
    SAVE_CHANGES    = 53

    def __init__(self, mb_port = 502, web_port = 80, verbose = True, installed_calibration_files = None, channel_names = None, channel_curves = None):
        super(Cabtr, self).__init__(0, [0x0] * 900)

        self.mb_port  = mb_port
        self.web_port = web_port

        if channel_names is None:
            channel_names = [
                                "TT69",
                                "TT23",
                                "TT91",
                                "TT92",
                                "TT93",
                                "TT94",
                                "TT30",
                                "TT49",
                            ]

        if channel_curves is None:
            channel_curves = [
                                "pt100.pol",
                                "pt100.pol",
                                "TT91X107576.cof",
                                "TT92X107575.cof",
                                "TT93X107574.cof",
                                "TT94X107573.cof",
                                "TT30X107512.cof",
                                "TT30X107512.cof",
                            ]

        self.channels = [
                            Channel(self, 1, channel_names[0], channel_curves[0]),
                            Channel(self, 2, channel_names[1], channel_curves[1]),
                            Channel(self, 3, channel_names[2], channel_curves[2]),
                            Channel(self, 4, channel_names[3], channel_curves[3]),
                            Channel(self, 5, channel_names[4], channel_curves[4]),
                            Channel(self, 6, channel_names[5], channel_curves[5]),
                            Channel(self, 7, channel_names[6], channel_curves[6]),
                            Channel(self, 8, channel_names[7], channel_curves[7]),
                        ]

        if installed_calibration_files is None:
            self.installed_calibration_files = list(set(channel_curves))
        else:
            self.installed_calibration_files = list(installed_calibration_files)

        self.__mb_server = self.__create_modbus()
        self.__mb_server.allow_reuse_address = True
        self.__wb_server = self.__create_web()
        self.__mb_thread = None
        self.__wb_thread = None

        # Model
        self.direct_setValues(0,  self.serialize("CABTR", str, 4))
        self.direct_setValues(24, self.serialize("1.0", str, 4))
        # Test values
        self.direct_setValues(58, self.serialize(0x1234, int, 1))
        self.direct_setValues(59, self.serialize(0x1A2B3C4D, int, 2))
        self.direct_setValues(61, self.serialize(0x47F12065, int, 2))

        for ch in self.channels:
            ch.serialize()

        if verbose:
            print("""CABTR Initialized
""")
            for ch in self.channels:
                print(ch)


    @staticmethod
    def serialize(values, typ = int, count = 1):
        try:
            if typ is int:
                if count == 1:
                    values = [ values ]
                elif count == 2:
                    values = [values & 0xFFFF, values >> 16]
                else:
                    raise RuntimeError("Cannot serialize", values)
            elif typ is str:
                tmp = values
                values = [0] * count
                count = len(tmp)
                for x in range(0, count // 2):
                    values[x] = (ord(tmp[x * 2 + 1]) << 8) | ord(tmp[x * 2])
                if count % 2:
                    # Zero based, no need to add + 1
                    values[count // 2] = ord(tmp[-1])
            else:
                raise RuntimeError("Cannot deserialize unknown type", typ)
        except Exception as e:
            print(e)
            raise

        return values


    @staticmethod
    def deserialize(values, typ):
        try:
            if typ is int:
                if len(values) == 1:
                    ret = values[0]
                elif len(values) == 2:
                    ret = (values[1] << 16) | values[0]
                else:
                    raise RuntimeError("Cannot deserialize", values)
            elif typ is str:
                ret = []
                for v in values:
                    ret.append(chr(v & 0xFF))
                    ret.append(chr(v >> 8))
                ret = "".join(ret)
            else:
                raise RuntimeError("Cannot deserialize unknown type", typ)
        except Exception as e:
            print(e)
            raise

#        print("Deserialized to ", ret)
        return ret


#    def getValues(self, address, count = 1):
#        return super(Cabtr, self).getValues(address, count)


    def direct_setValues(self, address, values):
        return super(Cabtr, self).setValues(address, values)


    def setValues(self, address, values):
#        print("setValues", address, values)

        for ch in self.channels:
            if ch.is_this(address):
                ch.deserialize(address, values)
                return

        # Channel related writes are done by the channel object
        super(Cabtr, self).setValues(address, values)

        if values[0] == 1:
            if address == self.COMMIT_CHANNELS:
#                print("Committing channel changes...")
                for ch in self.channels:
                    ch.commit()
                v = self.getValues(32, 1)[0]
                v = v | 64
                self.direct_setValues(32, v)

                #FIXME: check if we have to reset COMMIT_CHANNELS
            elif address == self.SAVE_CHANGES:
#                print("Saving changes...")
                v = self.getValues(32, 1)[0]
                v = v & ~64
                self.direct_setValues(32, v)


    def upload_calibration_file(self, calib_file):
        if calib_file not in self.installed_calibration_files:
            self.installed_calibration_files.append(calib_file)


    def start(self):
        self.__mb_thread = Thread(name = "Modbus", target = self.__mb_server.serve_forever)
        self.__mb_thread.start()

        self.__wb_thread = Thread(name = "Web", target = self.__wb_server.serve_forever)
        self.__wb_thread.start()


    def is_running(self):
        if self.__mb_thread is None and self.__wb_thread is None:
            return False

        return self.__mb_thread.is_alive() and self.__wb_thread.is_alive()


    def run(self):
        self.start()

        try:
            while True:
                sleep(10)
        except Exception:
            pass
        finally:
            self.stop()


    def stop(self):
        self.__mb_server.shutdown()
        self.__wb_server.shutdown()

        self.__mb_thread.join()
        self.__wb_thread.join()


    def __create_modbus(self):
        slave = ModbusSlaveContext(ir = self, hr = self, di = self, co = self, zero_mode = True)
        ctx = ModbusServerContext(slaves = slave, single = True)
        class CabtrModbusServer(modbus.ModbusTcpServer):
            allow_reuse_address = True
            def __init__(self, mb_port):
                super(CabtrModbusServer, self).__init__(context = ctx, address=("localhost", mb_port), allow_reuse_address = True)

            def shutdown(self):
                super(CabtrModbusServer, self).shutdown()
                self.server_close()

        return CabtrModbusServer(self.mb_port)


    def __create_web(self):
        class CabtrHTTPServer(HTTPServer):
            allow_reuse_address = True
            def __init__(self, cabtr, *args, **kw_args):
                super(CabtrHTTPServer, self).__init__(*args, **kw_args)
                self.__cabtr = cabtr

            def shutdown(self):
                super(CabtrHTTPServer, self).shutdown()
                self.server_close()

            def cabtr_channels(self):
                return self.__cabtr.channels

            def installed_calibration_files(self):
                return self.__cabtr.installed_calibration_files

            def upload_calibration_file(self, calib_file):
                return self.__cabtr.upload_calibration_file(calib_file)

        return CabtrHTTPServer(self, ("localhost", self.web_port), CabtrHTTPRequestHandler)



if __name__ == "__main__":
    Cabtr(5020, 8080).run()
